import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
  SafeAreaView,
  Linking
} from "react-native";
import RNFetchBlob from "rn-fetch-blob";
import {
  DocumentPicker,
  DocumentPickerUtil
} from "react-native-document-picker";
import { Toast, Colors } from "react-native-ui-lib";
import {
  AppInstalledChecker,
  CheckPackageInstallation
} from "react-native-check-app-install";
import FileViewer from "react-native-file-viewer";
import Papa from "papaparse";
import RNFS from "react-native-fs";
// import AppLink from 'react-native-app-link';
// "csv": {pkgName: "cn.wps.moffice_eng", urlScheme: "csv", urlParams: "app"},

class SaveCsv extends Component {
  state = {
    path: "",
    fileName: "",
    toastVisible: false,
    importMesssage: "",
    importError: false,
    importToast: false,
    importErrorMessage: ""
  };

  exportCsv = () => {
    const values = [
      ["build", "2017-11-05T05:40:35.515Z"],
      ["deploy", "2017-11-05T05:42:04.810Z"]
    ];

    const headerString =
      "Unit Name, Unit Type(Should be one of these 3: 'Vacant Plot' OR 'Flat' OR 'Villa'), Unit Dimension(Dimension of the particular unit eg: '1200'), Unit Calculation Type(Should be one of these 2: 'FlatRate' OR 'Dimension Based'),Unit Rate(eg: '1.55'), Occupancy Status(Should be one of these 3: 'Sold Owner Occupied Units' OR 'Sold Tenant Occupied Units' OR 'UnSold Tenant Occupied Units'), Owner FirstName, Owner LastName, Owner MobileNumber, Owner EmailID, Alternate MobileNumber, Alternate Email, BankName, IFSC, AccountNumber, AccountType(Should be one of these 2: 'Savings' OR 'Current'), Parking Lot Number, BlockName\n";
    //   const headerString = 'event,timestamp\n';
    const rowString = values.map(d => `${d[0]},${d[1]}\n`).join("");
    //   const csvString = `${headerString}${rowString}`;
    const csvString = `${headerString}`;

    let pathToWrite;

    if (Platform.OS === "android") {
      pathToWrite = `${RNFetchBlob.fs.dirs.DownloadDir}/oyespace_unit.csv`;
    } else {
      pathToWrite = RNFS.DocumentDirectoryPath + "/oyespace_unit.csv";
    }

    console.log("pathToWrite", pathToWrite);

    RNFS.writeFile(pathToWrite, csvString, "utf8")
      .then(success => {
        console.log(`wrote file ${pathToWrite}`);
        this.setState({ toastVisible: true, path: pathToWrite });
        // this.readCsv()
        // console.log(success)
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  readCsv = () => {
    const pathToWrite = `${RNFetchBlob.fs.dirs.DownloadDir}/${
      this.state.fileName
    }`;

    RNFS.readFile(this.state.path, "utf8")
      .then(data => {
        // console.log(data)
        let json = Papa.parse(data);

        console.log(json);
        console.log(json.data[1].length);

        let prevData = json.data[1];

        let newData = prevData.map(values => {
          str = values.replace(/\s+/g, "");
          return str.toLowerCase();
        });

        console.log(newData);

        if (json.data[1].length < 18) {
          this.setState({
            importToast: true,
            importMesssage:
              "Import Failed! Please make sure you fill the fields and try again.",
            importError: true
          });
        } else if (this.authenticateValues(newData)) {
          this.setState({
            importToast: true,
            importMesssage: "Import Successful",
            importError: false
          });
        } else {
          this.setState({
            importToast: true,
            importMesssage: this.state.importErrorMessage,
            importError: true
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  importCsv = () => {
    DocumentPicker.show(
      {
        filetype: [DocumentPickerUtil.allFiles()]
      },
      (error, res) => {
        // Android
        // console.log(
        //    res.uri,
        // );

        // console.log(
        //     res.type,
        //  );

        //  console.log(
        //     res.fileName,
        //  );

        //  console.log(
        //     res.fileSize
        // );

        console.log(error);

        if (error) {
          // alert('Please import a ')
        } else {
          this.readCsv();
          if (res.type !== "text/comma-separated-values") {
            alert("You can only import CSV files!");
          } else {
            this.setState({ path: res.uri, fileName: res.fileName });
            this.readCsv();
          }
        }

        // if(res) {
        //     FileViewer.open(res.uri)
        //     .then(() => {
        //       // success
        //     })
        //     .catch(_err => {
        //       // error
        //     });
        // }
      }
    );
  };

  authenticateValues = values => {
    console.log(values[4]);
    if (
      values[1] !== "flat" &&
      values[1] !== "vacantplot" &&
      values[1] !== "villa"
    ) {
      this.setState({
        importErrorMessage:
          "Import Failed, Unit Type Must Be Either 'VACANT PLOT', 'FLAT' OR 'VILLA'"
      });

      return false;
    } else if (values[3] !== "flatrate" && values[3] !== "dimensionbased") {
      this.setState({
        importErrorMessage:
          "Import Failed, Calculation Type Must Be Either 'FLAT RATE', OR 'DIMENSION BASED'"
      });

      return false;
    } else if (
      values[5] !== "soldowneroccupiedunits" &&
      values[5] !== "soldtenantoccupiedunits" &&
      values[5] !== "unsoldtenantoccupiedunits"
    ) {
      this.setState({
        importErrorMessage:
          "Import Failed,Occupancy Status Must Be Either 'SOLD OWNER OCCUPIED UNITS', 'SOLD TENANT OCCUPIED UNITS' OR 'UNSOLD TENANT OCCUPIED UNITS'"
      });

      return false;
    } else if (values[15] !== "savings" && values[15] !== "current") {
      this.setState({
        importErrorMessage:
          "Import Failed,Occupancy Status Must Be Either 'SAVINGS' OR 'CURRENT'"
      });

      return false;
    } else return true;
  };

  componentDidMount() {}

  render() {
    const action = [
      {
        label: "Open csv",
        backgroundColor: Colors.red40,
        onPress: () => {
          FileViewer.open(this.state.path)
            .then(() => {
              console.log("open");
              this.setState({ toastVisible: false });
            })
            .catch(error => {
              console.log(error);
              this.setState({ toastVisible: false });
              // error
            });
        }
      }
    ];

    return (
      <SafeAreaView style={styles.container}>
        <Toast
          messageStyle={{
            textAlign: "center",
            justifyContent: "center",
            alignItems: "center",
            fontWeight: "700"
          }}
          style={{ flex: 1, width: "100%" }}
          visible={this.state.toastVisible}
          position={"top"}
          message="Exported Successfully"
          autoDismiss={200}
          onDismiss={() => this.setState({ toastVisible: false })}
          // onDismiss={() => this.setState({showTopToast: false})}
          // allowDismiss={showDismiss}
          actions={action}
        />
        <Toast
          messageStyle={{
            textAlign: "center",
            justifyContent: "center",
            alignItems: "center",
            fontWeight: "700"
          }}
          style={{ flex: 1, width: "100%" }}
          visible={this.state.importToast}
          position={"top"}
          backgroundColor={
            this.state.importError ? Colors.red40 : Colors.blue40
          }
          message={this.state.importMesssage}
          autoDismiss={3000}
          onDismiss={() => this.setState({ importToast: false })}
          // allowDismiss={showDismiss}
          // actions={action}
        />
        <View style={{ flex: 1, justifyContent: "space-around" }}>
          <Button
            onPress={this.exportCsv}
            title="Tap to export CSV"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />

          <Button
            onPress={this.importCsv}
            title="Import CSV"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
        </View>
        <View style={{ flex: 1 }} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#fff",
    justifyContent: "space-around",
    alignItems: "center"
  }
});

export default SaveCsv;
