/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
// import { StyleSheet, View, PermissionsAndroid } from 'react-native';
import {View, Text,  StyleSheet, PermissionsAndroid, Alert,Platform } from 'react-native';
import SaveCsv from './src/screens/SaveCsv';
// import console = require('console');


export default class App extends Component {

  componentDidMount = () => {
    //Checking for the permission just after component loaded
      async function requestAllPermissions() {
        try {
          if (Platform.OS === "android") {
            const userResponse = await PermissionsAndroid.requestMultiple([
              PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            ]);
            return userResponse;
          }
        } catch (err) {
          Warning(err);
        }
        return null;
      }
     if (Platform.OS === 'android') {
         //Calling the permission function
         requestAllPermissions();
     }else{
        //  alert('IOS device found');
     }
  }
 
  
  render() {
    return (
      <View style={styles.container}>
        <SaveCsv />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
